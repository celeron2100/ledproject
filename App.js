import React, { useState } from "react";
import {
  StyleSheet,
  Button,
  View,
  SafeAreaView,
  Text,
  Alert,
  Modal,
  Pressable,
} from "react-native";

const Separator = () => <View style={styles.separator} />;

const App = () => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <SafeAreaView style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>選擇要連接的藍芽設備</Text>
            <Separator />
            <Separator />
            <Separator />
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>關閉</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <Separator />
      <View>
        <Text style={styles.title}>點擊按鈕來進行藍芽LED設備的連接</Text>
        <View style={styles.fixToText}>
          <Button
            title="左腳藍芽連接"
            color="#228b22"
            //onPress={() => Alert.alert('Left button pressed')}
            onPress={() => setModalVisible(true)}
          />
          <Button
            title="右腳藍芽連接"
            color="#4169e1"
            // onPress={() => Alert.alert('Right button pressed')}
            onPress={() => setModalVisible(true)}
          />
        </View>
        <Separator />
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginHorizontal: 16,
  },
  title: {
    textAlign: "center",
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default App;
